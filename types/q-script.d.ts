/**
 * MARK: CSS Module
 * For css's importing in Typescript environment
 */ 

declare module '*.module.css' {
  const classList: { [key: string]: string };
  export default classList;
}

declare module '*.module.scss' {
  const classList: { [key: string]: string };
  export default classList;
}

declare module '*.module.sass' {
  const classList: { [key: string]: string };
  export default classList;
}