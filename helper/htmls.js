const fs = require('fs')

/**
 * @private
 * @param {Object<string, any>} attributes
 */
function generateAttributes(attributes) {
  const attributeArray = Object.entries(attributes)
  return attributeArray.map(([key, value]) => ` ${key}="${value}"`).join('\n')
}

/**
 * @private
 * @param {string} template
 * @param {string} title
 * @returns {string}
 */
function insertTitle(template, title) {
  if (title) {
    const reg = /(<title>).*(<title>)/
    if (reg.exec(template)) {
      template = template.replace(reg, `$1${title}$2`)
    } else {
      template = template.replace(
        /<head>/,
        `$&<meta charset="utf-8"/><title>${title}</title>\n`
      )
    }
  }

  return template
}

/**
 * @private
 * @param {string} template
 * @param {Array<import('rollup').EmittedChunk>} jsList
 * @param {string} publicPath
 * @param {Object<string, any>=} attributes
 * @returns {string}
 */
function insertJavaScript(template, jsList, publicPath, attributes) {
  jsList = Array.from(jsList)
  const entry = jsList.find((js) => js.isEntry)

  jsList.forEach(
    (js) =>
      js.type == 'asset' &&
      (template = template.replace(
        /<\/head>/,
        `<script type="application/javascript" src="/${js.fileName}"></script>$&`
      ))
  )

  const list = entry
    ? jsList
        .filter(
          (js) =>
            !entry.dynamicImports.includes(js.fileName) &&
            js.type === 'chunk' &&
            !js.implicitlyLoadedBefore.length &&
            !js.imports.length
        )
        .concat([entry])
    : jsList
  const scriptContent = list
    .map(({ fileName }) => `System.import('/${fileName}')`)
    .join(';')
  const script = `<script>${scriptContent}</script>$&`
  return template.replace(/<\/body>/, script)
}

/**
 * @private
 * @param {string} template
 * @param {Array<import('rollup').EmittedAsset>} cssList
 * @param {string} publicPath
 * @param {Object<string, any>=} attributes
 * @returns {string}
 */
function insertCss(template, cssList, publicPath, attributes) {
  const list = cssList
    .map(
      ({ fileName }) =>
        `<link rel="stylesheet" type="text/css" href="/${fileName}"${generateAttributes(
          attributes || {}
        )}/>`
    )
    .join('\n')
  return template.replace(/<\/head>/, `${list}$&`)
}

/**
 * To generate a template-generator method based on the html file at provided path
 * @param {string} templatePath
 * @returns {Function}
 */
function generateTemplate(templatePath) {
  return ({
    attributes: { html, script, link },
    files: { js, css },
    publicPath,
    title,
  }) => {
    let template = fs.readFileSync(templatePath).toString()
    template = template.replace(/\n */g, '\n')
    template = template.replace(
      /(<(?!\/).*html.*)(>)/,
      `$1${generateAttributes(html || {})}$2`
    )
    template = insertCss(template, css || [], publicPath, link)
    template = insertJavaScript(template, js || [], publicPath, script)

    return template
  }
}

module.exports = { generateTemplate }
