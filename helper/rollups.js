const { promises: fs } = require('fs')
const path = require('path')

const replace = require('@rollup/plugin-replace')
const alias = require('@rollup/plugin-alias')
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const commonjs = require('@rollup/plugin-commonjs')
const html = require('@rollup/plugin-html')

const babel = require('@rollup/plugin-babel').default
const babelPresetEnv = require('@babel/preset-env').default
const babelPresetReact = require('@babel/preset-react').default
const babelPresetTypescript = require('@babel/preset-typescript').default

const babelPluginClassProperties =
  require('@babel/plugin-proposal-class-properties').default
const babelPluginTransform = require('@babel/plugin-transform-runtime').default

const postcss = require('rollup-plugin-postcss')
const autoprefixer = require('autoprefixer')

const { injectSystemjs } = require('./injectSystemjs')
const { terser } = require('rollup-plugin-terser')

const { generateTemplate } = require('./htmls.js')
const {
  SOURCE_PATH,
  APP_PATH,
  OUTPUT_PATH,
  PUBLIC_PATH,
  ENTRY,
  IS_TYPESCRIPT_ENV,
  ALIAS_PATH,
} = require('../config/const.js')

const VALID_ENTRY_EXTNAME_LIST = ['.js', 'jsx']
const EXTRA_BABLE_PRESET_LIST = []

if (IS_TYPESCRIPT_ENV) {
  VALID_ENTRY_EXTNAME_LIST.push('.ts', '.tsx')
  EXTRA_BABLE_PRESET_LIST.push(babelPresetTypescript)
}

/**
 * To generate js chunk config of vendors
 * @returns {Promise<Record<string, string[]>>}
 */
async function manualChunks() {
  /** @type {Record<string, string[]>} */
  const result = {}
  const { dependencies } = JSON.parse(
    await fs.readFile(path.join(APP_PATH, 'package.json'), 'utf-8')
  )
  if (dependencies) {
    result.vendor = Object.keys(dependencies)
  }

  return result
}

/**
 * To generate plugin config
 * @returns {Promise<import('rollup').Plugin[]>}
 */
async function plugin() {
  const entries = { ...ALIAS_PATH }
  for (const a in entries) {
    entries[a] = APP_PATH + entries[a]
  }

  return [
    replace({ 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) }),
    alias({ entries: entries }),
    nodeResolve({
      extensions: VALID_ENTRY_EXTNAME_LIST,
    }),
    commonjs(),
    postcss({
      extract: true,
      plugins: [autoprefixer()],
    }),
    babel({
      extensions: VALID_ENTRY_EXTNAME_LIST,
      babelHelpers: 'runtime',
      exclude: './node_modules/**',
      presets: [
        [
          babelPresetEnv,
          {
            modules: false,
            // useBuiltIns: 'usage',
            // corejs: '3',
            exclude: ['transform-typeof-symbol'],
          },
        ],
        [babelPresetReact, { runtime: 'automatic' }],
        ...EXTRA_BABLE_PRESET_LIST,
      ],
      plugins: [
        [
          babelPluginClassProperties,
          {
            loose: true,
          },
        ],
        [
          babelPluginTransform,
          {
            corejs: 3,
            absoluteRuntime: true,
          },
        ],
      ],
    }),
    injectSystemjs(),
    terser(),
    html({
      template: generateTemplate(`${PUBLIC_PATH}/${ENTRY}.html`),
      fileName: `${ENTRY}.html`,
    }),
  ]
}

/**
 * To generate a rollup config object
 * @param isDevelopMode {boolean=}
 * @returns {Promise<import('rollup').RollupOptions>} rollup config
 */
async function generateConfig(isDevelopMode) {
  // SECTION: input

  let input = (await fs.readdir(SOURCE_PATH)).find(
    (fileName) =>
      fileName.includes(ENTRY) &&
      VALID_ENTRY_EXTNAME_LIST.includes(path.extname(fileName))
  )
  input = path.join(SOURCE_PATH, input)

  return {
    preserveEntrySignatures: false,
    input: input,
    manualChunks: await manualChunks(),
    plugins: await plugin(),
    output: {
      dir: OUTPUT_PATH,
      format: 'system',
    },
  }
}

module.exports = { generateConfig }
