const { HOST, ENTRY } = require('../config/const.js')

/**
 * @private
 * @param {string} port
 */
function consoleServerLink(port) {
  const localLink = `http://localhost:${port}`
  const ipLink = `http://${HOST}:${port}`

  let message = '\x1b[32m'
  message += '🚀... Start Server:\n\n'
  message += `  # ${localLink}\n`
  message += `  # ${ipLink}\n`
  message += '\n\n\x1b[0m'
  process.stdout.write(message)
}

/**
 * @private
 * @param {{
 *  code: 'BUNDLE_START',
 *  input: import('rollup').InputOption,
 *  output: readonly string[]
 * }} event
 */
function consoleBundleStart(event) {
  let message = ''
  message += `🚛... Start Bundling: ${event.input}`
  message += '\n\x1b[0m'
  process.stdout.write(message)
}

/**
 * @private
 * @param {{
 *  code: 'ERROR',
 *  error: import('rollup').RollupError
 * }} event
 */
function consoleError(event) {
  const err = event.error
  let message = '\x1b[33m'
  message += `🚒... Error: ${err.message}\n\n`
  if (err.loc) {
    message += `  @ ${err.loc.file}\n`
  }
  message += '\x1b[0m'
  if (err.frame) {
    message += err.frame
  }
  message += '\n\x1b[33m'
  message += `⛑ ...`
  message += '\n\x1b[0m'
  process.stdout.write(message)
}

module.exports = { consoleBundleStart, consoleError, consoleServerLink }
