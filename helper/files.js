const fs = require('fs')
const pathTool = require('path')
const { APP_PATH } = require('../config/const.js')

/**
 * To delete all files in the folder of provided path
 * @param {string} path folder path relative to APP_PATH
 */
async function deleteFolder(path) {
  let fileList = []
  try {
    fileList = await fs.promises.readdir(pathTool.resolve(APP_PATH, path))
  } catch(err) {
    fileList = []
  }

  for (const file of fileList) {
    fs.promises.unlink(pathTool.resolve(APP_PATH, path, file))
  }
}

module.exports = { deleteFolder }
