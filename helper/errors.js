function popError(...args) {
  const error = args.pop()
  const message = args.pop() || ''
  const isContinue = args.pop() || false

  if (message) {
    process.stderr.write(`${message}\n`)
  }

  if (error) {
    process.stderr.write(`${error.stack || error}\n`)
  } else {
    process.stderr.write('FAIL NO REASON...\n')
  }

  if (!isContinue) {
    process.exit(1)
  }
}

module.exports = { popError }
