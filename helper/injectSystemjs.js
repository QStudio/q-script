const {
  promises: { readFile },
} = require('fs')
const { APP_PATH, PACKAGE_PATH } = require('../config/const.js')

function injectSystemjs() {
  return {
    name: 'inject-systemjs',
    async renderStart() {
      const systemjs = await readFile(
        `${APP_PATH}/node_modules/systemjs/dist/s.min.js`,
        'utf-8'
      ).catch(() =>
        readFile(
          `${PACKAGE_PATH}/node_modules/systemjs/dist/system.min.js`,
          'utf-8'
        )
      )
      this.emitFile({
        type: 'asset',
        fileName: 's.min.js',
        source: systemjs,
      })
    },
  }
}

module.exports = { injectSystemjs }
