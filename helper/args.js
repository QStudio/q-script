const parseArgs = require('minimist')

/**
 * convert `'camelCase'` to be `CAMEL_CASE`
 * @param {string} val
 * @returns {string | null}
 */
function convertCamelCase(val) {
  if (/![A-Z]/.test(val)) {
    return null
  }

  val = val.charAt(0).toLowerCase() + val.slice(1)
  return val.replace(/([A-Z])/g, '_$1').toUpperCase()
}

/**
 * convert `'snack-case'` to be `SNACK_CASE`
 * @param {string} val
 * @returns {string | null}
 */
function convertSnackCase(val) {
  if (!/-/.test(val)) {
    return null
  }
  return val.split('-').join('_').toUpperCase()
}

/**
 * @param {string[]} argv process.argv
 * @param {Record<string, any>} env process.env
 * @returns {Record<string, any>}
 */
function getArgs(argv, env) {
  const args = Object.entries(parseArgs(argv.slice(2))).reduce(
    (collection, entry) => {
      const [key, value] = entry
      collection[
        convertCamelCase(key) || convertSnackCase(key) || key.toUpperCase()
      ] = value
      return collection
    },
    {}
  )

  args.SCRIPT = args._[0]

  return {
    ...env,
    ...args,
    PORT:
      typeof args.PORT === 'string' || typeof args.PORT === 'number'
        ? args.PORT
        : env.PORT,
  }
}

module.exports = {
  getArgs,
}
