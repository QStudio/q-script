const express = require('express')
const pathTool = require('path')
const { promises: fs } = require('fs')
const {
  OUTPUT_PATH,
  ENTRY,
  RELATIVE_PATH,
  SOURCE_PATH,
  CONFIG_PATH,
  SERVER_CONFIG,
} = require('../config/const.js')

/**
 * To init a express server to host the developing project
 * @private
 */
async function generateApp() {
  const app = express()
  app.use(express.static(OUTPUT_PATH))
  app.use(RELATIVE_PATH.SOURCE, express.static(SOURCE_PATH))

  let config
  try {
    const dir = await fs.readdir(CONFIG_PATH)
    const filePath = pathTool.resolve(
      CONFIG_PATH,
      dir.find((d) => d.includes(SERVER_CONFIG))
    )

    let module
    if (pathTool.extname(filePath) === '.mjs') {
      module = await import(filePath).default
    } else {
      module = require(filePath)
    }

    config = module
  } catch (e) {
    config = {
      get: [],
      post: [],
      use: [],
    }
  }

  for (const { path, handler } of config.get || []) {
    app.get(path, handler)
  }

  for (const { path, handler } of config.post || []) {
    app.post(path, handler)
  }

  for (const { path, handler } of config.use || []) {
    const params = [path, handler].filter((param) => !!param)
    app.use(...params)
  }

  return app
}

module.exports = { generateApp }
