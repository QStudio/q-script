const { generateApp } = require('../helper/apps.js')
const { OUTPUT_PATH, ENTRY } = require('../config/const.js')
const { consoleServerLink } = require('../helper/consoles')
const build = require('./build')

// MARK: Main

/**
 * @typedef {object} ServeArgs
 * @property {boolean=} CURRENT_ASSETS run develop server without build if assets already exists
 * @property {(string | number)=} PORT on which run develop server
 */

/**
 * To start a node based server for development of q-script's caller
 * @param {ServeArgs} args
 * @returns {import('http').Server}
 */
async function serve(args) {
  const port = args.PORT || '8080'
  if (
    !args.CURRENT_ASSETS ||
    !fs.existsSync(path.join(APP_PATH, RELATIVE_PATH.OUTPUT, `${ENTRY}.html`))
  ) {
    await build()
  }

  const app = await generateApp()
  app.get('*', (request, response) => {
    response.sendFile(`${OUTPUT_PATH}/${ENTRY}.html`)
  })

  app.listen(port)
  consoleServerLink(port)
}

module.exports = serve
