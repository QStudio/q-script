const { watch } = require('rollup')
const { generateConfig } = require('../helper/rollups.js')
const { generateApp } = require('../helper/apps.js')
const {
  consoleBundleStart,
  consoleServerLink,
  consoleError,
} = require('../helper/consoles')
const { ENTRY } = require('../config/const.js')

// MARK: Watcher

/** @private */
class LoadingLock {
  constructor() {
    this.promise = new Promise((rs) => (this.unlock = rs))
  }

  then(fn) {
    return this.promise.then(fn)
  }

  unlock(val) {
    this.unlock(val)
  }
}

function convertFileListToMap(list) {
  const result = {}
  list.forEach((file) => (result[file.fileName] = file))

  return result
}

/**
 * @typeof {object} Config
 * @property {(event) => void} onBuildStart
 * @property {(event) => void} onBuildEnd
 * @property {(event) => void} onError
 */

/** @private */
class Watcher {
  /**
   * @param {Config=} config
   */
  constructor(config = {}) {
    this.config = config
    this.lock = new LoadingLock()
    this.initWatcher()
  }

  /** @private */
  async initWatcher() {
    const {
      onBuildStart = () => {},
      onBuildEnd = () => {},
      onError = () => {},
    } = this.config
    const { output, ...input } = await generateConfig(true)
    if (Array.isArray(output)) {
      output.forEach((o) => (o.sourcemap = true))
    } else {
      output.sourcemap = true
    }

    const watcher = watch({
      ...input,
      output,
      watch: {
        skipWrite: true,
      },
    })

    watcher.on('event', (event) => {
      switch (event.code) {
        case 'BUNDLE_START':
          this.lock = new LoadingLock()
          onBuildStart(event)
          break
        case 'BUNDLE_END':
          event.result
            .generate(output)
            .then((d) => this.lock.unlock(convertFileListToMap(d.output)))
          onBuildEnd(event)
          break
        case 'ERROR':
          onError(event)
      }
    })
  }

  /**
   * @param {string} fileName
   * @returns {Promise<string>}
   */
  getFile(fileName) {
    return this.lock.then((map) => {
      fileName = (/^\/(.*)/.exec(fileName) || [])[1]
      if (!map[fileName]) {
        fileName = `${ENTRY}.html`
      }

      const file = map[fileName]
      if (!file || (!file.code && !file.source)) {
        return Promise.reject(new Error('404'))
      }

      let content = (file.code || file.source).toString()
      if (file.type === 'chunk' && file.map) {
        content += `\n//# sourceMappingURL=${file.map.toUrl()}`
      }

      return content
    })
  }
}

// MARK: Main

/**
 * @typedef {object} StartArgs
 * @property {(string | number)=} PORT on which run develop server
 */

/**
 * To start a node based server for development of q-script's caller
 * @param {StartArgs} args
 * @returns {import('http').Server}
 */
async function start(args) {
  const port = args.PORT || '8080'

  const watcher = new Watcher({
    onBuildStart: consoleBundleStart,
    onBuildEnd: consoleServerLink.bind(null, port),
    onError: consoleError,
  })

  const app = await generateApp()
  app.get('*', async (req, res) => {
    const file = await watcher
      .getFile(req.originalUrl)
      .catch((err) => `ERROR ${err.message}`)
    res.send(file)
  })
  app.listen(port)

  consoleServerLink(port)
  return app
}

module.exports = start
