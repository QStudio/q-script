const { rollup } = require('rollup')
const { generateConfig } = require('../helper/rollups.js')
const { popError } = require('../helper/errors')
const { deleteFolder } = require('../helper/files.js')
const { OUTPUT_PATH } = require('../config/const.js')

/**
 * To build a bundle of q-script's caller
 */
async function build() {
  const deleted = deleteFolder(OUTPUT_PATH).catch(
    popError.bind(null, true, 'DELETE FAIL...')
  )
  const { output, ...input } = await generateConfig().catch(
    popError.bind(null, 'CONFIG FAIL...')
  )
  const bundle = await rollup(input).catch(
    popError.bind(null, 'BUNDLE FAIL...')
  )
  await deleted

  let outputList
  if (Array.isArray(output)) {
    outputList = output
  } else {
    outputList = [output]
  }

  await Promise.all(outputList.map((o) => bundle.write(o))).catch(
    popError.bind(null, 'WRITE FAIL...')
  )
}

module.exports = build
