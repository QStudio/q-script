const { promises: fs } = require('fs')
const path = require('path')
const {
  RELATIVE_PATH,
  ALIAS_PATH,
  PACKAGE_PATH,
} = require('../config/const.js')

/**
 * @param {string} from folder path
 * @param {string} to folder path
 */
async function copySync(from, to) {
  const state = await fs.lstat(from)
  if (state.isFile()) {
    fs.copyFile(from, to)
    return
  }

  const arr = await fs.readdir(from)
  arr.forEach((sub) => {
    copySync(path.join(from, sub), path.join(to, sub))
  })
}

/**
 * @private
 * @param {string} path
 * @returns {boolea}
 */
function isSlashStartPath(path) {
  return /^\.\//.test(path) || /^\//.test(path)
}

/**
 * @private
 * @param {string} a
 * @param {string} b
 * @returns {boolean}
 */
function isSamePath(a, b) {
  if (!isSlashStartPath(a)) {
    a = `./${a}`
  }
  if (!isSlashStartPath(b)) {
    b = `./${a}`
  }
  return a === b
}

/**
 * get tab context from json string
 * @private
 * @param {?string=} val
 * @returns {string}
 */
function getTabString(val) {
  if (!val) {
    return '\t'
  }

  let tab = /^\{( {0,})[a-zA-Z]*:/.exec(val)
  if (!tab) {
    return '\t'
  }

  return tab[1]
}

async function install() {
  // NOTE: The APP_PATH is different while install method is called
  const APP_PATH = process.env.INIT_CWD
  const TSCONFIG_PATH = path.join(APP_PATH, 'tsconfig.json')

  let config = await fs.readFile(TSCONFIG_PATH, 'utf8').catch(() => null)
  if (!config) {
    return
  }
  const tab = getTabString(config)
  config = JSON.parse(config)

  const preset = JSON.parse(
    await fs.readFile(path.join(PACKAGE_PATH, 'tsconfig.json'), 'utf8')
  )

  const paths = {}
  for (const alias in ALIAS_PATH) {
    paths[`${alias}/*`] = [`.${ALIAS_PATH[alias]}/*`]
  }

  config['compilerOptions'] = {
    ...preset['compilerOptions'],
    ...config['compilerOptions'],
    lib: Array.from(
      new Set(
        preset['compilerOptions']['lib'],
        config['compilerOptions']['lib']
      )
    ),
    paths: {
      ...preset['compilerOptions']['paths'],
      ...config['compilerOptions']['paths'],
      ...paths,
    },
  }

  const includeSet = new Set(preset['include'], config['include'])
  includeSet.add(`.${RELATIVE_PATH.SOURCE}/`)
  includeSet.add(`.${RELATIVE_PATH.TYPES}/`)
  config['include'] = Array.from(includeSet).reduce((prev, curr) => {
    if (!prev.includes(isSamePath.bind(null, curr))) {
      prev.push(curr)
    }

    return prev
  }, [])

  fs.writeFile(TSCONFIG_PATH, JSON.stringify(config, null, tab))
  copySync(
    path.join(PACKAGE_PATH, RELATIVE_PATH.TYPES),
    path.join(APP_PATH, RELATIVE_PATH.TYPES)
  )
}

module.exports = install
