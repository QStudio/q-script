# QScript

A React app bundle tool based on [Rollup](https://rollupjs.org/guide/en/).



## Install

```
npm i react react-dom
npm i -D @QStudio/q-script
```



## Folder Structure

```
Root
|
|- build
|
|- public
|  |- index.html
|
|- src
|  |- index.js
|
|- qscript
   |- server.mjs
```

#### public folder

The folder for public, static files.

Files in the folder will be in the root of domain.

A `index.html` file is needed in this folder as web's entrance.

#### src folder

The folder for source code. Files in the folder will be bundled.

A `index.js` file is needed in this folder as web's entrance.

#### qscript folder

The folder for **QScript**'s config.

QScript use es module resolution, so every JavaScript file's extension need to be `mjs`.



## Scripts

#### `npm run start`

Run the app in a develop server and files changed will be relflect into the server every reload.

The app will be served at `http://localhost:8080`.

| Options            | Type                | Description                                                  |
| ------------------ | ------------------- | ------------------------------------------------------------ |
| `--port`           | `string` | `number` | The port on which the development app listen                 |

#### `npm run serve`

Build and serve the app in a develop server.

The app will be served at `http://localhost:8080`.

| Options            | Type                | Description                                                  |
| ------------------ | ------------------- | ------------------------------------------------------------ |
| `--current-assets` | `boolean`           | Whether rebuild assets' or not before development app established |
| `--port`           | `string` | `number` | The port on which the development app listen                 |

#### `npm run build`

Builds/Bundle the app in the production mode to the **build** folder.



## Style

- It is possible to import `css` file in `js`:

  ```js
  import style from './style.css'

  document.body.innerHtml = `
     <div class="${style.cssSelectorDefinedInStyleFile}"></div>
  `
  ```

- If style file's extension `.module.css`, QScript will deal the file as CSS module.

- Sass is supported, the file with `.scss` or `.sass` extension, then will be compiled automatically.

  > sass or node-sass need to be installed: `npm i -D sass` 



## Alias Import Path

WIP



## Config

#### Development Server Config

It is possible to customize development server's route by editing `server.mjs`.

`ServerConfig` needs to be `export default` in `server.mjs`

```ts
import { RequestHandler } from 'express'

interface RouteConfig {
  path: string
  handler: RequestHandler
}

interface MiddleWareConfig {
  path?: string
  handler: RequestHandler
}

interface ServerConfig {
  get: RouteConfig[]
  post: RouteConfig[]
  use: MiddleWareConfig[]
}
```



