# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.10.3](https://gitlab.com/qstudio/q-script/compare/v0.10.2...v0.10.3) (2022-02-01)


### Features

* inject systemjs as indenpendent js file ([4d96257](https://gitlab.com/qstudio/q-script/commit/4d962577aa2836b8e9cfcae6029bc69f5a8da412))
