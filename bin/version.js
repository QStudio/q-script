#!/usr/bin/env node

const tag = process.env.CI_COMMIT_TAG
if (!tag) {
  return
}

const fs = require('fs')
const packPath = './package.json'
const pack = JSON.parse(fs.readFileSync(packPath, 'utf-8'))
pack.version = /^v(.*)/.exec(tag)[1] || pack.version
fs.writeFileSync(packPath, JSON.stringify(pack))
