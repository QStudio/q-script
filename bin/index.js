#!/usr/bin/env node

const { getArgs } = require('../helper/args')
const { popError } = require('../helper/errors')
const build = require('../scripts/build')
const start = require('../scripts/start')
const install = require('../scripts/install')
const serve = require('../scripts/serve')

const binArgs = getArgs(process.argv, process.env)

/**
 * @enum {string}
 */
const Script = {
  BUILD: 'build',
  START: 'start',
  INSTALL: 'install',
  SERVE: 'serve',
}

let result
switch (binArgs.SCRIPT) {
  case Script.START:
    result = start(binArgs)
    break
  case Script.INSTALL:
    result = install(binArgs)
    break
  case Script.SERVE:
    result = serve(binArgs)
    break
  case Script.BUILD:
  default:
    result = build(binArgs)
}
result.catch(popError)
