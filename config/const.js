const { networkInterfaces } = require('os')
const { readdirSync } = require('fs')
const { resolve } = require('path')

/**
 * IP address of localhost
 */
const HOST =
  (
    Object.values(networkInterfaces())
      .reduce((result, infoList) => {
        return result.concat(
          infoList.filter(
            (info) => info.family === 'IPv4' && info.internal === false
          )
        )
      }, [])
      .pop() || {}
  ).address || '0.0.0.0'

// dirname(fileURLToPath(import.meta.url))
const PACKAGE_PATH = resolve(__dirname, '..')

/**
 * The root path of q-script's caller.
 */
const APP_PATH = process.cwd()

/**
 * The relative folder path of q-script.
 */
const RELATIVE_PATH = Object.freeze({
  /**
   * The folder for source code of q-script's caller.
   */
  SOURCE: '/src',

  /**
   * The folder for bundle output of q-script's caller.
   */
  OUTPUT: '/build',

  /**
   * The folder for static assets of q-script's caller.
   * such as html template.
   */
  PUBLIC: '/public',

  /**
   * The folder for q-script's config.
   */
  CONFIG: '/qscript',

  TYPES: '/types',
})

/**
 * The absolute path of bundler output folder
 */
const OUTPUT_PATH = APP_PATH + RELATIVE_PATH.OUTPUT

/**
 * The absolute path of source code folder
 */
const SOURCE_PATH = APP_PATH + RELATIVE_PATH.SOURCE

/**
 * The absolute path of static assets folder
 */
const PUBLIC_PATH = APP_PATH + RELATIVE_PATH.PUBLIC

/**
 * The absolute path of mock server folder
 */
const CONFIG_PATH = APP_PATH + RELATIVE_PATH.CONFIG

/**
 * The entry point of bundled project (q-script's caller).
 * There need html file in public path and javascript file in source path to be named as ENTRY.
 * such as 'index.html' in public path and 'index.js' in source path
 */
const ENTRY = 'index'

/**
 * The enrty point of mock server's plugin.
 * Need to default config object.
 * The exported array's child will be used as:
 * @example
 * // NOTE: For `server.mjs`
 * export default {
 *    get: [
 *        { path: '/path' , handler(request, respond) {} }
 *    ],
 *
 *    post: [
 *        { path: '/path' , handler(request, respond) {} }
 *    ],
 *
 *    use: [
 *       { path: '/path', handler() {} }
 *    ]
 * }
 *
 * // NOTE: For `server.js`
 * module.exports = {
 *    get: [
 *        { path: '/path' , handler(request, respond) {} }
 *    ],
 *
 *    post: [
 *        { path: '/path' , handler(request, respond) {} }
 *    ],
 *
 *    use: [
 *       { path: '/path', handler() {} }
 *    ]
 * }
 */
const SERVER_CONFIG = 'server'

/**
 * The flag to check whether q-script's caller is typescript environment or not.
 */
const IS_TYPESCRIPT_ENV = (() => {
  let isTypescript = false
  if (readdirSync(APP_PATH).includes('tsconfig.json')) {
    isTypescript = true
  }

  return isTypescript
})()

/**
 * The alias path for modules' importing.
 */
const ALIAS_PATH = {
  /**
   * The source path of q-script's caller
   */
  '@src': RELATIVE_PATH.SOURCE,
}

module.exports = {
  HOST,
  PACKAGE_PATH,
  APP_PATH,
  OUTPUT_PATH,
  SOURCE_PATH,
  PUBLIC_PATH,
  CONFIG_PATH,
  ENTRY,
  IS_TYPESCRIPT_ENV,
  ALIAS_PATH,
  RELATIVE_PATH,
  SERVER_CONFIG,
}
